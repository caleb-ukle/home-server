#! /bin/zsh
set -e

NOW=$(date +%y-%m-%d)


echo "Backing up config dir for $NOW"
tar -czf config.$NOW.tar.gz --exclude="Cache" --exclude="Caches" /opt/plex/config

echo "Moving to long term storage"
mv config.$NOW.tar.gz /mnt/media/backup

echo $?

ll /mnt/media/backup/config.$NOW.tar.gz

echo "Done"
